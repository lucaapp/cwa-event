/**
 * Copyright (c) 2020-2021 Deutsche Telekom AG and SAP SE or 
 * an SAP affiliate company.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *          https://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the LICENSE for the specific language governing permissions 
 * and limitations under the License.
 * 
 * The "Corona-Warn-App" logo is a registered trademark of The Press and 
 * Information Office of the Federal Government. For more information 
 * please see bundesregierung.de.
 */

import moment from 'moment';
// @ts-ignore
import { encode } from 'uint8-to-base64';
// @ts-ignore
import { proto } from './lib/trace_location_pb';

import { QRCodeDataType } from './types';
import { CROWD_NOTIFIER_PUBLIC_KEY } from './constants';

/** validates if all parameters are set correctly
 * @param {*} qrCodeData
 * @returns true if all is sane
 */
export const validateQRData = (qrCodeData: QRCodeDataType) => {
  let errors = 0;

  if (!qrCodeData.locationType) {
    errors++;
  }

  if (!qrCodeData.description || !qrCodeData.description.length) {
    errors++;
  } else if (qrCodeData.description.length > 100) {
    errors++;
  }

  if (!qrCodeData.address || !qrCodeData.address.length) {
    errors++;
  } else if (qrCodeData.address.length > 100) {
    errors++;
  }
  if (!qrCodeData.defaultcheckinlengthMinutes) {
    errors++;
  }

  if (qrCodeData.locationType >= 9 || qrCodeData.locationType === 2) {
    const timeReg = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;
    const dateReg = /^\d{2}.\d{2}.\d{4}$/;

    if (!qrCodeData.starttime || !qrCodeData.startdate) {
      errors++;
    } else {
      if (
        !dateReg.test(qrCodeData.startdate) ||
        !moment(qrCodeData.startdate.split('.').reverse().join('-')).isValid()
      ) {
        errors++;
      }

      if (!timeReg.test(qrCodeData.starttime)) {
        errors++;
      }
    }

    if (!qrCodeData.endtime || !qrCodeData.enddate) {
      errors++;
    } else {
      if (
        !dateReg.test(qrCodeData.enddate) ||
        !moment(qrCodeData.enddate.split('.').reverse().join('-')).isValid()
      ) {
        errors++;
      }

      if (!timeReg.test(qrCodeData.endtime)) {
        errors++;
      }
    }
  }

  return errors === 0;
};

export const generateSerializedPayload = (
  qrCodeData: QRCodeDataType
): Uint8Array => {
  const locationData = new proto.CWALocationData();
  locationData.setVersion(1);
  locationData.setType(qrCodeData.locationType);
  locationData.setDefaultcheckinlengthinminutes(
    qrCodeData.defaultcheckinlengthMinutes
  );

  const crowdNotifierData = new proto.CrowdNotifierData();
  crowdNotifierData.setVersion(1);
  crowdNotifierData.setPublickey(CROWD_NOTIFIER_PUBLIC_KEY);

  const seed = new Uint8Array(16);
  crypto.getRandomValues(seed);
  crowdNotifierData.setCryptographicseed(seed);

  const traceLocation = new proto.TraceLocation();
  traceLocation.setVersion(1);
  traceLocation.setDescription(qrCodeData.description);
  traceLocation.setAddress(qrCodeData.address);

  if (qrCodeData.locationType >= 9 || qrCodeData.locationType === 2) {
    traceLocation.setStarttimestamp(
      moment(
        // @ts-ignore
        qrCodeData.startdate.reverse().join('-') + ' ' + qrCodeData.starttime
      ).format('X')
    );
    traceLocation.setEndtimestamp(
      moment(
        // @ts-ignore
        qrCodeData.enddate.reverse().join('-') + ' ' + qrCodeData.endtime
      ).format('X')
    );
  }

  const payload = new proto.QRCodePayload();
  payload.setLocationdata(traceLocation);
  payload.setCrowdnotifierdata(crowdNotifierData);

  payload.setVendordata(locationData.serializeBinary());
  payload.setVersion(1);

  return payload.serializeBinary();
};
/**
 * Generates CWA payload as string
 *
 * @param {*} qrCodeData
 * @returns payload as string
 */
export const generateQRPayload = (qrCodeData: QRCodeDataType) => {
  const qrContent = encode(generateSerializedPayload(qrCodeData))
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');

  return qrContent;
};

export const LOCATION_TYPES = proto.TraceLocationType;
