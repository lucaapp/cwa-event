export type QRCodeDataType = {
  description: string;
  address: string;
  defaultcheckinlengthMinutes: string;
  locationType: number;
  startdate: string;
  starttime: string;
  enddate: string;
  endtime: string;
};
