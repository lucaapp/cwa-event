# Luca CWA Event

[luca](https://luca-app.de) ensures a data protection-compliant, decentralized
encryption of your data, undertakes the obligation to record contact data for
events and gastronomy, relieves the health authorities through digital, lean,
and integrated processes to enable efficient and complete tracing.

This service contains the source code of the Luca CWA Event package.
Purpose of this package, is to port the event qr code registration logic from the corona warn app (CWA) to a browser-compatible easy-to-use package.
The goal is to take all the parameters like event description and time and generate the payload data that is going to be appended to the URL behind a QR Code.


The Luca CWA Event package is used in [Luca Web Services](https://gitlab.com/lucaapp/web/).

## Acknowledgements 
The logic and code used in this repository are adapted from the CWA repositories that offer the logic to generate the CWA QR code payload. The primary repositories usedg are
* [cwa-event-qr-code](https://github.com/corona-warn-app/cwa-event-qr-code)
* [cwa-website](https://github.com/corona-warn-app/cwa-website)


## Changelog

An overview of all releases can be found
[here](https://gitlab.com/lucaapp/cwa-event/-/blob/master/CHANGELOG.md).

## Issues & Support

Please [create an issue](https://gitlab.com/lucaapp/cwa-event/-/issues) for
suggestions or problems related to this application. For general questions,
please check out our [FAQ](https://www.luca-app.de/faq/) or contact our support
team at [hello@luca-app.de](mailto:hello@luca-app.de).

## License

The Luca CWA Event is Free Software (Open Source) and is distributed
with a number of components with compatible licenses.

```
SPDX-License-Identifier: Apache-2.0

SPDX-FileCopyrightText: 2021 culture4life GmbH <https://luca-app.de>
```

For details see
* [license file](https://gitlab.com/lucaapp/cwa-event/-/blob/master/LICENSE)