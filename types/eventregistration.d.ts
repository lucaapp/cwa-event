import { QRCodeDataType } from './types';
export declare const validateQRData: (qrCodeData: QRCodeDataType) => boolean;
export declare const generateSerializedPayload: (qrCodeData: QRCodeDataType) => Uint8Array;
export declare const generateQRPayload: (qrCodeData: QRCodeDataType) => any;
export declare const LOCATION_TYPES: any;
