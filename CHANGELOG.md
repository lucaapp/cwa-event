# Changelog

### 1.0.1 (2021-05-24)
* Remove debugging logs

### 1.0.0 (2021-05-24)

* initial public release
