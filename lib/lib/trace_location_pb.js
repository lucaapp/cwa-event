"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const google_protobuf_1 = __importDefault(require("google-protobuf/google-protobuf"));
var jspb = require('google-protobuf');
var goog = jspb;
var global = function () {
    return this;
};
goog.exportSymbol('proto.CWALocationData', null, global);
goog.exportSymbol('proto.CrowdNotifierData', null, global);
goog.exportSymbol('proto.QRCodePayload', null, global);
goog.exportSymbol('proto.TraceLocation', null, global);
goog.exportSymbol('proto.TraceLocationType', null, global);
google_protobuf_1.default.QRCodePayload = function (opt_data) {
    jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(google_protobuf_1.default.QRCodePayload, jspb.Message);
if (goog.DEBUG && !COMPILED) {
    google_protobuf_1.default.QRCodePayload.displayName = 'proto.QRCodePayload';
}
google_protobuf_1.default.TraceLocation = function (opt_data) {
    jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(google_protobuf_1.default.TraceLocation, jspb.Message);
if (goog.DEBUG && !COMPILED) {
    google_protobuf_1.default.TraceLocation.displayName = 'proto.TraceLocation';
}
google_protobuf_1.default.CrowdNotifierData = function (opt_data) {
    jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(google_protobuf_1.default.CrowdNotifierData, jspb.Message);
if (goog.DEBUG && !COMPILED) {
    google_protobuf_1.default.CrowdNotifierData.displayName = 'proto.CrowdNotifierData';
}
google_protobuf_1.default.CWALocationData = function (opt_data) {
    jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(google_protobuf_1.default.CWALocationData, jspb.Message);
if (goog.DEBUG && !COMPILED) {
    google_protobuf_1.default.CWALocationData.displayName = 'proto.CWALocationData';
}
if (jspb.Message.GENERATE_TO_OBJECT) {
    google_protobuf_1.default.QRCodePayload.prototype.toObject = function (opt_includeInstance) {
        return google_protobuf_1.default.QRCodePayload.toObject(opt_includeInstance, this);
    };
    google_protobuf_1.default.QRCodePayload.toObject = function (includeInstance, msg) {
        var f, obj = {
            version: jspb.Message.getFieldWithDefault(msg, 1, 0),
            locationdata: (f = msg.getLocationdata()) &&
                google_protobuf_1.default.TraceLocation.toObject(includeInstance, f),
            crowdnotifierdata: (f = msg.getCrowdnotifierdata()) &&
                google_protobuf_1.default.CrowdNotifierData.toObject(includeInstance, f),
            vendordata: msg.getVendordata_asB64(),
        };
        if (includeInstance) {
            obj.$jspbMessageInstance = msg;
        }
        return obj;
    };
}
google_protobuf_1.default.QRCodePayload.deserializeBinary = function (bytes) {
    var reader = new jspb.BinaryReader(bytes);
    var msg = new google_protobuf_1.default.QRCodePayload();
    return google_protobuf_1.default.QRCodePayload.deserializeBinaryFromReader(msg, reader);
};
google_protobuf_1.default.QRCodePayload.deserializeBinaryFromReader = function (msg, reader) {
    while (reader.nextField()) {
        if (reader.isEndGroup()) {
            break;
        }
        var field = reader.getFieldNumber();
        switch (field) {
            case 1:
                var value = reader.readUint32();
                msg.setVersion(value);
                break;
            case 2:
                var value = new google_protobuf_1.default.TraceLocation();
                reader.readMessage(value, google_protobuf_1.default.TraceLocation.deserializeBinaryFromReader);
                msg.setLocationdata(value);
                break;
            case 3:
                var value = new google_protobuf_1.default.CrowdNotifierData();
                reader.readMessage(value, google_protobuf_1.default.CrowdNotifierData.deserializeBinaryFromReader);
                msg.setCrowdnotifierdata(value);
                break;
            case 4:
                var value = reader.readBytes();
                msg.setVendordata(value);
                break;
            default:
                reader.skipField();
                break;
        }
    }
    return msg;
};
google_protobuf_1.default.QRCodePayload.prototype.serializeBinary = function () {
    var writer = new jspb.BinaryWriter();
    google_protobuf_1.default.QRCodePayload.serializeBinaryToWriter(this, writer);
    return writer.getResultBuffer();
};
google_protobuf_1.default.QRCodePayload.serializeBinaryToWriter = function (message, writer) {
    var f = undefined;
    f = message.getVersion();
    if (f !== 0) {
        writer.writeUint32(1, f);
    }
    f = message.getLocationdata();
    if (f != null) {
        writer.writeMessage(2, f, google_protobuf_1.default.TraceLocation.serializeBinaryToWriter);
    }
    f = message.getCrowdnotifierdata();
    if (f != null) {
        writer.writeMessage(3, f, google_protobuf_1.default.CrowdNotifierData.serializeBinaryToWriter);
    }
    f = message.getVendordata_asU8();
    if (f.length > 0) {
        writer.writeBytes(4, f);
    }
};
google_protobuf_1.default.QRCodePayload.prototype.getVersion = function () {
    return jspb.Message.getFieldWithDefault(this, 1, 0);
};
google_protobuf_1.default.QRCodePayload.prototype.setVersion = function (value) {
    return jspb.Message.setProto3IntField(this, 1, value);
};
google_protobuf_1.default.QRCodePayload.prototype.getLocationdata = function () {
    return jspb.Message.getWrapperField(this, google_protobuf_1.default.TraceLocation, 2);
};
google_protobuf_1.default.QRCodePayload.prototype.setLocationdata = function (value) {
    return jspb.Message.setWrapperField(this, 2, value);
};
google_protobuf_1.default.QRCodePayload.prototype.clearLocationdata = function () {
    return this.setLocationdata(undefined);
};
google_protobuf_1.default.QRCodePayload.prototype.hasLocationdata = function () {
    return jspb.Message.getField(this, 2) != null;
};
google_protobuf_1.default.QRCodePayload.prototype.getCrowdnotifierdata = function () {
    return jspb.Message.getWrapperField(this, google_protobuf_1.default.CrowdNotifierData, 3);
};
google_protobuf_1.default.QRCodePayload.prototype.setCrowdnotifierdata = function (value) {
    return jspb.Message.setWrapperField(this, 3, value);
};
google_protobuf_1.default.QRCodePayload.prototype.clearCrowdnotifierdata = function () {
    return this.setCrowdnotifierdata(undefined);
};
google_protobuf_1.default.QRCodePayload.prototype.hasCrowdnotifierdata = function () {
    return jspb.Message.getField(this, 3) != null;
};
google_protobuf_1.default.QRCodePayload.prototype.getVendordata = function () {
    return jspb.Message.getFieldWithDefault(this, 4, '');
};
google_protobuf_1.default.QRCodePayload.prototype.getVendordata_asB64 = function () {
    return jspb.Message.bytesAsB64(this.getVendordata());
};
google_protobuf_1.default.QRCodePayload.prototype.getVendordata_asU8 = function () {
    return jspb.Message.bytesAsU8(this.getVendordata());
};
google_protobuf_1.default.QRCodePayload.prototype.setVendordata = function (value) {
    return jspb.Message.setProto3BytesField(this, 4, value);
};
if (jspb.Message.GENERATE_TO_OBJECT) {
    google_protobuf_1.default.TraceLocation.prototype.toObject = function (opt_includeInstance) {
        return google_protobuf_1.default.TraceLocation.toObject(opt_includeInstance, this);
    };
    google_protobuf_1.default.TraceLocation.toObject = function (includeInstance, msg) {
        var f, obj = {
            version: jspb.Message.getFieldWithDefault(msg, 1, 0),
            description: jspb.Message.getFieldWithDefault(msg, 2, ''),
            address: jspb.Message.getFieldWithDefault(msg, 3, ''),
            starttimestamp: jspb.Message.getFieldWithDefault(msg, 5, 0),
            endtimestamp: jspb.Message.getFieldWithDefault(msg, 6, 0),
        };
        if (includeInstance) {
            obj.$jspbMessageInstance = msg;
        }
        return obj;
    };
}
google_protobuf_1.default.TraceLocation.deserializeBinary = function (bytes) {
    var reader = new jspb.BinaryReader(bytes);
    var msg = new google_protobuf_1.default.TraceLocation();
    return google_protobuf_1.default.TraceLocation.deserializeBinaryFromReader(msg, reader);
};
google_protobuf_1.default.TraceLocation.deserializeBinaryFromReader = function (msg, reader) {
    while (reader.nextField()) {
        if (reader.isEndGroup()) {
            break;
        }
        var field = reader.getFieldNumber();
        switch (field) {
            case 1:
                var value = reader.readUint32();
                msg.setVersion(value);
                break;
            case 2:
                var value = reader.readString();
                msg.setDescription(value);
                break;
            case 3:
                var value = reader.readString();
                msg.setAddress(value);
                break;
            case 5:
                var value = reader.readUint64();
                msg.setStarttimestamp(value);
                break;
            case 6:
                var value = reader.readUint64();
                msg.setEndtimestamp(value);
                break;
            default:
                reader.skipField();
                break;
        }
    }
    return msg;
};
google_protobuf_1.default.TraceLocation.prototype.serializeBinary = function () {
    var writer = new jspb.BinaryWriter();
    google_protobuf_1.default.TraceLocation.serializeBinaryToWriter(this, writer);
    return writer.getResultBuffer();
};
google_protobuf_1.default.TraceLocation.serializeBinaryToWriter = function (message, writer) {
    var f = undefined;
    f = message.getVersion();
    if (f !== 0) {
        writer.writeUint32(1, f);
    }
    f = message.getDescription();
    if (f.length > 0) {
        writer.writeString(2, f);
    }
    f = message.getAddress();
    if (f.length > 0) {
        writer.writeString(3, f);
    }
    f = message.getStarttimestamp();
    if (f !== 0) {
        writer.writeUint64(5, f);
    }
    f = message.getEndtimestamp();
    if (f !== 0) {
        writer.writeUint64(6, f);
    }
};
google_protobuf_1.default.TraceLocation.prototype.getVersion = function () {
    return jspb.Message.getFieldWithDefault(this, 1, 0);
};
google_protobuf_1.default.TraceLocation.prototype.setVersion = function (value) {
    return jspb.Message.setProto3IntField(this, 1, value);
};
google_protobuf_1.default.TraceLocation.prototype.getDescription = function () {
    return jspb.Message.getFieldWithDefault(this, 2, '');
};
google_protobuf_1.default.TraceLocation.prototype.setDescription = function (value) {
    return jspb.Message.setProto3StringField(this, 2, value);
};
google_protobuf_1.default.TraceLocation.prototype.getAddress = function () {
    return jspb.Message.getFieldWithDefault(this, 3, '');
};
google_protobuf_1.default.TraceLocation.prototype.setAddress = function (value) {
    return jspb.Message.setProto3StringField(this, 3, value);
};
google_protobuf_1.default.TraceLocation.prototype.getStarttimestamp = function () {
    return jspb.Message.getFieldWithDefault(this, 5, 0);
};
google_protobuf_1.default.TraceLocation.prototype.setStarttimestamp = function (value) {
    return jspb.Message.setProto3IntField(this, 5, value);
};
google_protobuf_1.default.TraceLocation.prototype.getEndtimestamp = function () {
    return jspb.Message.getFieldWithDefault(this, 6, 0);
};
google_protobuf_1.default.TraceLocation.prototype.setEndtimestamp = function (value) {
    return jspb.Message.setProto3IntField(this, 6, value);
};
if (jspb.Message.GENERATE_TO_OBJECT) {
    google_protobuf_1.default.CrowdNotifierData.prototype.toObject = function (opt_includeInstance) {
        return google_protobuf_1.default.CrowdNotifierData.toObject(opt_includeInstance, this);
    };
    google_protobuf_1.default.CrowdNotifierData.toObject = function (includeInstance, msg) {
        var f, obj = {
            version: jspb.Message.getFieldWithDefault(msg, 1, 0),
            publickey: msg.getPublickey_asB64(),
            cryptographicseed: msg.getCryptographicseed_asB64(),
        };
        if (includeInstance) {
            obj.$jspbMessageInstance = msg;
        }
        return obj;
    };
}
google_protobuf_1.default.CrowdNotifierData.deserializeBinary = function (bytes) {
    var reader = new jspb.BinaryReader(bytes);
    var msg = new google_protobuf_1.default.CrowdNotifierData();
    return google_protobuf_1.default.CrowdNotifierData.deserializeBinaryFromReader(msg, reader);
};
google_protobuf_1.default.CrowdNotifierData.deserializeBinaryFromReader = function (msg, reader) {
    while (reader.nextField()) {
        if (reader.isEndGroup()) {
            break;
        }
        var field = reader.getFieldNumber();
        switch (field) {
            case 1:
                var value = reader.readUint32();
                msg.setVersion(value);
                break;
            case 2:
                var value = reader.readBytes();
                msg.setPublickey(value);
                break;
            case 3:
                var value = reader.readBytes();
                msg.setCryptographicseed(value);
                break;
            default:
                reader.skipField();
                break;
        }
    }
    return msg;
};
google_protobuf_1.default.CrowdNotifierData.prototype.serializeBinary = function () {
    var writer = new jspb.BinaryWriter();
    google_protobuf_1.default.CrowdNotifierData.serializeBinaryToWriter(this, writer);
    return writer.getResultBuffer();
};
google_protobuf_1.default.CrowdNotifierData.serializeBinaryToWriter = function (message, writer) {
    var f = undefined;
    f = message.getVersion();
    if (f !== 0) {
        writer.writeUint32(1, f);
    }
    f = message.getPublickey_asU8();
    if (f.length > 0) {
        writer.writeBytes(2, f);
    }
    f = message.getCryptographicseed_asU8();
    if (f.length > 0) {
        writer.writeBytes(3, f);
    }
};
google_protobuf_1.default.CrowdNotifierData.prototype.getVersion = function () {
    return jspb.Message.getFieldWithDefault(this, 1, 0);
};
google_protobuf_1.default.CrowdNotifierData.prototype.setVersion = function (value) {
    return jspb.Message.setProto3IntField(this, 1, value);
};
google_protobuf_1.default.CrowdNotifierData.prototype.getPublickey = function () {
    return jspb.Message.getFieldWithDefault(this, 2, '');
};
google_protobuf_1.default.CrowdNotifierData.prototype.getPublickey_asB64 = function () {
    return jspb.Message.bytesAsB64(this.getPublickey());
};
google_protobuf_1.default.CrowdNotifierData.prototype.getPublickey_asU8 = function () {
    return jspb.Message.bytesAsU8(this.getPublickey());
};
google_protobuf_1.default.CrowdNotifierData.prototype.setPublickey = function (value) {
    return jspb.Message.setProto3BytesField(this, 2, value);
};
google_protobuf_1.default.CrowdNotifierData.prototype.getCryptographicseed = function () {
    return jspb.Message.getFieldWithDefault(this, 3, '');
};
google_protobuf_1.default.CrowdNotifierData.prototype.getCryptographicseed_asB64 = function () {
    return jspb.Message.bytesAsB64(this.getCryptographicseed());
};
google_protobuf_1.default.CrowdNotifierData.prototype.getCryptographicseed_asU8 = function () {
    return jspb.Message.bytesAsU8(this.getCryptographicseed());
};
google_protobuf_1.default.CrowdNotifierData.prototype.setCryptographicseed = function (value) {
    return jspb.Message.setProto3BytesField(this, 3, value);
};
if (jspb.Message.GENERATE_TO_OBJECT) {
    google_protobuf_1.default.CWALocationData.prototype.toObject = function (opt_includeInstance) {
        return google_protobuf_1.default.CWALocationData.toObject(opt_includeInstance, this);
    };
    google_protobuf_1.default.CWALocationData.toObject = function (includeInstance, msg) {
        var f, obj = {
            version: jspb.Message.getFieldWithDefault(msg, 1, 0),
            type: jspb.Message.getFieldWithDefault(msg, 2, 0),
            defaultcheckinlengthinminutes: jspb.Message.getFieldWithDefault(msg, 3, 0),
        };
        if (includeInstance) {
            obj.$jspbMessageInstance = msg;
        }
        return obj;
    };
}
google_protobuf_1.default.CWALocationData.deserializeBinary = function (bytes) {
    var reader = new jspb.BinaryReader(bytes);
    var msg = new google_protobuf_1.default.CWALocationData();
    return google_protobuf_1.default.CWALocationData.deserializeBinaryFromReader(msg, reader);
};
google_protobuf_1.default.CWALocationData.deserializeBinaryFromReader = function (msg, reader) {
    while (reader.nextField()) {
        if (reader.isEndGroup()) {
            break;
        }
        var field = reader.getFieldNumber();
        switch (field) {
            case 1:
                var value = reader.readUint32();
                msg.setVersion(value);
                break;
            case 2:
                var value = reader.readEnum();
                msg.setType(value);
                break;
            case 3:
                var value = reader.readUint32();
                msg.setDefaultcheckinlengthinminutes(value);
                break;
            default:
                reader.skipField();
                break;
        }
    }
    return msg;
};
google_protobuf_1.default.CWALocationData.prototype.serializeBinary = function () {
    var writer = new jspb.BinaryWriter();
    google_protobuf_1.default.CWALocationData.serializeBinaryToWriter(this, writer);
    return writer.getResultBuffer();
};
google_protobuf_1.default.CWALocationData.serializeBinaryToWriter = function (message, writer) {
    var f = undefined;
    f = message.getVersion();
    if (f !== 0) {
        writer.writeUint32(1, f);
    }
    f = message.getType();
    if (f !== 0.0) {
        writer.writeEnum(2, f);
    }
    f = message.getDefaultcheckinlengthinminutes();
    if (f !== 0) {
        writer.writeUint32(3, f);
    }
};
google_protobuf_1.default.CWALocationData.prototype.getVersion = function () {
    return jspb.Message.getFieldWithDefault(this, 1, 0);
};
google_protobuf_1.default.CWALocationData.prototype.setVersion = function (value) {
    return jspb.Message.setProto3IntField(this, 1, value);
};
google_protobuf_1.default.CWALocationData.prototype.getType = function () {
    return jspb.Message.getFieldWithDefault(this, 2, 0);
};
google_protobuf_1.default.CWALocationData.prototype.setType = function (value) {
    return jspb.Message.setProto3EnumField(this, 2, value);
};
google_protobuf_1.default.CWALocationData.prototype.getDefaultcheckinlengthinminutes = function () {
    return jspb.Message.getFieldWithDefault(this, 3, 0);
};
google_protobuf_1.default.CWALocationData.prototype.setDefaultcheckinlengthinminutes = function (value) {
    return jspb.Message.setProto3IntField(this, 3, value);
};
google_protobuf_1.default.TraceLocationType = {
    LOCATION_TYPE_UNSPECIFIED: 0,
    LOCATION_TYPE_PERMANENT_OTHER: 1,
    LOCATION_TYPE_TEMPORARY_OTHER: 2,
    LOCATION_TYPE_PERMANENT_RETAIL: 3,
    LOCATION_TYPE_PERMANENT_FOOD_SERVICE: 4,
    LOCATION_TYPE_PERMANENT_CRAFT: 5,
    LOCATION_TYPE_PERMANENT_WORKPLACE: 6,
    LOCATION_TYPE_PERMANENT_EDUCATIONAL_INSTITUTION: 7,
    LOCATION_TYPE_PERMANENT_PUBLIC_BUILDING: 8,
    LOCATION_TYPE_TEMPORARY_CULTURAL_EVENT: 9,
    LOCATION_TYPE_TEMPORARY_CLUB_ACTIVITY: 10,
    LOCATION_TYPE_TEMPORARY_PRIVATE_EVENT: 11,
    LOCATION_TYPE_TEMPORARY_WORSHIP_SERVICE: 12,
};
module.exports = {
    proto: google_protobuf_1.default,
};
