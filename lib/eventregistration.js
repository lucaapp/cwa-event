"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOCATION_TYPES = exports.generateQRPayload = exports.generateSerializedPayload = exports.validateQRData = void 0;
const moment_1 = __importDefault(require("moment"));
const uint8_to_base64_1 = require("uint8-to-base64");
const trace_location_pb_1 = require("./lib/trace_location_pb");
const constants_1 = require("./constants");
const validateQRData = (qrCodeData) => {
    let errors = 0;
    if (!qrCodeData.locationType) {
        errors++;
    }
    if (!qrCodeData.description || !qrCodeData.description.length) {
        errors++;
    }
    else if (qrCodeData.description.length > 100) {
        errors++;
    }
    if (!qrCodeData.address || !qrCodeData.address.length) {
        errors++;
    }
    else if (qrCodeData.address.length > 100) {
        errors++;
    }
    if (!qrCodeData.defaultcheckinlengthMinutes) {
        errors++;
    }
    if (qrCodeData.locationType >= 9 || qrCodeData.locationType === 2) {
        const timeReg = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;
        const dateReg = /^\d{2}.\d{2}.\d{4}$/;
        if (!qrCodeData.starttime || !qrCodeData.startdate) {
            errors++;
        }
        else {
            if (!dateReg.test(qrCodeData.startdate) ||
                !moment_1.default(qrCodeData.startdate.split('.').reverse().join('-')).isValid()) {
                errors++;
            }
            if (!timeReg.test(qrCodeData.starttime)) {
                errors++;
            }
        }
        if (!qrCodeData.endtime || !qrCodeData.enddate) {
            errors++;
        }
        else {
            if (!dateReg.test(qrCodeData.enddate) ||
                !moment_1.default(qrCodeData.enddate.split('.').reverse().join('-')).isValid()) {
                errors++;
            }
            if (!timeReg.test(qrCodeData.endtime)) {
                errors++;
            }
        }
    }
    return errors === 0;
};
exports.validateQRData = validateQRData;
const generateSerializedPayload = (qrCodeData) => {
    const locationData = new trace_location_pb_1.proto.CWALocationData();
    locationData.setVersion(1);
    locationData.setType(qrCodeData.locationType);
    locationData.setDefaultcheckinlengthinminutes(qrCodeData.defaultcheckinlengthMinutes);
    const crowdNotifierData = new trace_location_pb_1.proto.CrowdNotifierData();
    crowdNotifierData.setVersion(1);
    crowdNotifierData.setPublickey(constants_1.CROWD_NOTIFIER_PUBLIC_KEY);
    const seed = new Uint8Array(16);
    crypto.getRandomValues(seed);
    crowdNotifierData.setCryptographicseed(seed);
    const traceLocation = new trace_location_pb_1.proto.TraceLocation();
    traceLocation.setVersion(1);
    traceLocation.setDescription(qrCodeData.description);
    traceLocation.setAddress(qrCodeData.address);
    if (qrCodeData.locationType >= 9 || qrCodeData.locationType === 2) {
        traceLocation.setStarttimestamp(moment_1.default(qrCodeData.startdate.reverse().join('-') + ' ' + qrCodeData.starttime).format('X'));
        traceLocation.setEndtimestamp(moment_1.default(qrCodeData.enddate.reverse().join('-') + ' ' + qrCodeData.endtime).format('X'));
    }
    const payload = new trace_location_pb_1.proto.QRCodePayload();
    payload.setLocationdata(traceLocation);
    payload.setCrowdnotifierdata(crowdNotifierData);
    payload.setVendordata(locationData.serializeBinary());
    payload.setVersion(1);
    return payload.serializeBinary();
};
exports.generateSerializedPayload = generateSerializedPayload;
const generateQRPayload = (qrCodeData) => {
    const qrContent = uint8_to_base64_1.encode(exports.generateSerializedPayload(qrCodeData))
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=+$/, '');
    return qrContent;
};
exports.generateQRPayload = generateQRPayload;
exports.LOCATION_TYPES = trace_location_pb_1.proto.TraceLocationType;
